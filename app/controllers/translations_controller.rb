class TranslationsController < ApplicationController

  def index
    @translations = Translation.all
  end

  def show
    if params[:id].nil?
      @translation = Translation.order("RANDOM()").first
    else
      redirect_to show_translation_path
    end
  end

  def new
  end
  
  def create
    return render action: "new" if params[:file].nil?
    uploaded_io = params[:file]
    path = "public/uploads/#{uploaded_io.original_filename}"
    File.open(Rails.root.join('public', 'uploads',uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
    end
    return render :text => "File have other path" if Translation.create_from_file(path).nil?
    redirect_to translations_path
  end

  def update
    @translation = Translation.where(id: params[:id]).first
    @translation.update(translation_count: (@translation.translation_count + 1))
    if params[:success] == true 
      @translation.update(success_translation_count: @translation.success_translation_count + 1)
    end
    #render json: { success: true }
    redirect_to show_translation_path
  end

  def destroy 
    @translation = Translation.find(params[:id])
    @translation.destroy
    render json: { success: true }
  end

end
