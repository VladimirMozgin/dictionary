class Admin::LanguagesController < Admin::ApplicationController

  def index
    @languages = Language.all
  end

  def show
    unless @language = Language.where(id: params[:id]).first
      render text: "Page not found", status: 404
    end
  end

  def new
    @language = Language.new
  end

  def create
    @language = Language.create(languages_params)
    if @language.errors.empty?
      redirect_to language_path(@language)
    else 
      render "new"
    end
  end

  def edit
    @language = Language.where(id: params[:id]).first
  end

  def update
    if @language.update(languages_params)
      redirect_to @percent, notice: 'Percent was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @language = Language.where(id: params[:id]).first
    @language.destroy
  end

  private

    def languages_params
      params.require(:language).permit(:name, :key)
    end
end
