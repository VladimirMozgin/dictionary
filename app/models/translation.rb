class Translation < ActiveRecord::Base
  belongs_to :word_language, class_name: "Language", inverse_of: :words, foreign_key: "word_language_id"
  belongs_to :translation_language, class_name: "Language", inverse_of: :translations, foreign_key: "translation_language_id"

  
  def self.create_from_file(path)
    extension = path.scan(/([^\.]+)$/)[0][0]
    case extension 
    when "xlsx"
      s = Roo::Excelx.new(path)
      s.default_sheet = s.sheets.first
      (1..s.last_row).each do |row|
        Translation.create(word: s.cell(row,1), translation: s.cell(row,2), word_language_id: 1, translation_language_id: 2)
      end
    when "csv"
      s = Roo::CSV.new(path)
      s.default_sheet = s.sheets.first
      (1..s.last_row).each do |row|
        Translation.create(word: s.cell(row,1), translation: s.cell(row,2), word_language_id: 1, translation_language_id: 2)
      end
    when "xls"
      s = Roo::Spreadsheet.open(path)
      s.each do |row|
        Translation.create(word: row[0], translation: row[1], word_language_id: 1, translation_language_id: 2)
      end
    else
    end
  end

  def percent
    if translation_count == 0
      return 0
    else
      (success_translation_count.to_f / translation_count * 100).round(2)
    end
  end

end
