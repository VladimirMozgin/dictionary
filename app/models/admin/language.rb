class Language < ActiveRecord::Base
  has_many :words, class_name: "Translation", foreign_key: "word_language_id"
  has_many :translations, class_name: "Translation", foreign_key: "translation_language_id"
end
