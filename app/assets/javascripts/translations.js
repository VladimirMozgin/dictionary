jQuery(function($){
  $(".show-word").hide();
  $(".btn-translate").hide();
  $(".btn-not-translate").hide();
  $(".btn-show-word").click(function(){
    $(".text-show-word").hide();
    $(".btn-show-word").removeClass("btn-success").addClass("btn-info");
    $(".show-word").show();
    $(".btn-translate").show();
    $(".btn-not-translate").show();
  });
});

$(function($) {
  $(".deleteAction").click( function(){
    var current_translation_tr = $(this).closest('tr');
    if(confirm("Are you sure?")){
      $.ajax({
        url: '/translations/' + current_translation_tr.attr('data-translation-id'),
        type: 'POST',
        data: { _method: 'DELETE'},
        success: function(){
          current_translation_tr.fadeOut(200);
        }
      });
    };
  });
});

$(function($){
  $(".btn-translate").click( function(){
    var current_translation = $(this).closest('div');
    $.ajax({
      url: '/translations/' + current_translation.attr('data-translation-id'),
      type: 'POST',
      data: { success : true, _method: 'PUT'},
      success: function(){
      }
    });
  });
});

$(function($){
  $(".btn-not-translate").click( function(){
    var current_translation = $(this).closest('div');
    $.ajax({
      url: '/translations/' + current_translation.attr('data-translation-id'),
      type: 'POST',
      data: { success : false, _method: 'PUT'},
      success: function(){
      }
    });
  });
});