class CreateTranslations < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.string :word_en
      t.string :word_ru
      t.timestamps
    end
  end
end
