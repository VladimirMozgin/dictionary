class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.string :title
      t.belongs_to :translation
      t.timestamps
    end
  end
end
