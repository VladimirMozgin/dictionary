class RemoveDetailsFromLanguages < ActiveRecord::Migration
  def change
    remove_column :languages, :title, :string
    remove_column :languages, :translation_id, :string
  end
end
