class AddWordLanguageIdAndTranslationLanguageIdToTranslations < ActiveRecord::Migration
  def change
    add_column :translations, :word_language_id, :integer
    add_column :translations, :translation_language_id, :integer
  end
end
