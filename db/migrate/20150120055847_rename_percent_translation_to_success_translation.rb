class RenamePercentTranslationToSuccessTranslation < ActiveRecord::Migration
  def change
    rename_column :translations, :percent_success_translation, :success_translation_count
  end
end
