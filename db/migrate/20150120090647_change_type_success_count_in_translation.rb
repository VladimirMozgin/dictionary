class ChangeTypeSuccessCountInTranslation < ActiveRecord::Migration
  def change
    change_column :translations, :success_translation_count, :integer
  end
end
