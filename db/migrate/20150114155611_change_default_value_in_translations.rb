class ChangeDefaultValueInTranslations < ActiveRecord::Migration
  def change
    change_column_default :translations, :translation_count, 0
    change_column_default :translations, :percent_success_translation, 0
  end
end
