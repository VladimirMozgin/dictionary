class RenameDetailsInTranslation < ActiveRecord::Migration
  def change
    rename_column :translations, :word_en, :word
    rename_column :translations, :word_ru, :translation 
  end
end
