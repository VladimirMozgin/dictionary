class AddDetailsToTranslations < ActiveRecord::Migration
  def change
    add_column :translations, :translation_count, :integer
    add_column :translations, :percent_success_translation, :float
  end
end
