require "roo"
require "spreadsheet"

puts "Input path like test.csv, test.xls, test.xlsx"
path = gets.chomp
extension = path.scan(/([^\.]+)$/)[0][0]
translate_en_ru = {}

case extension
when "xlsx"
  begin
    s = Roo::Excelx.new(path)
    s.default_sheet = s.sheets.first
    (1..s.last_row).each do |row|
      translate_en_ru[s.cell(row,1)] = s.cell(row,2)
    end
  rescue 
    puts "Error path"
  end  
when "csv"
  begin
    s = Roo::CSV.new(path)
    s.default_sheet = s.sheets.first
    (1..s.last_row).each do |row|
      translate_en_ru[s.cell(row,1)] = s.cell(row,2)
    end
  rescue
    puts "Error path"
  end 
when "xls"
  begin
    s = Roo::Spreadsheet.open(path)
    s.each do |row|
      translate_en_ru[row[0]]= row[1]
    end
  rescue 
    puts "Error path"
  end
else
  puts "file have other type"
end

p translate_en_ru unless translate_en_ru.empty?